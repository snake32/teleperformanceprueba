import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-registro-horas-extras',
  templateUrl: './registro-horas-extras.component.html',
  styleUrls: ['./registro-horas-extras.component.css']
})
export class RegistroHorasExtrasComponent implements OnInit {

  frmRegHoraExtra : FormGroup;

  constructor() { }

  ngOnInit() {
    this.frmRegHoraExtra = new FormGroup({
      txtDocumento: new FormControl('', []),
      txtNombreApellido: new FormControl('', []),
      dtFechaInicio: new FormControl('', []),
      dtFechaFin: new FormControl('', []),
      dtInicioTurno: new FormControl('', []),
      dtFinTurno: new FormControl('', []),
      dtInicioHE: new FormControl('', []),
      dtFinHE: new FormControl('', []),
      txtMotivoHE: new FormControl('', [])
    });
  }

  get txtDocumento() { return this.frmRegHoraExtra.get('txtDocumento')}
  get txtDoctxtNombreApellidoumento() { return this.frmRegHoraExtra.get('txtNombreApellido')}
  get dtFechaInicio() { return this.frmRegHoraExtra.get('dtFechaInicio')}
  get dtFechaFin() { return this.frmRegHoraExtra.get('dtFechaFin')}
  get dtInicioTurno() { return this.frmRegHoraExtra.get('dtInicioTurno')}
  get dtFinTurno() { return this.frmRegHoraExtra.get('dtFinTurno')}
  get dtInicioHE() { return this.frmRegHoraExtra.get('dtInicioHE')}
  get dtFinHE() { return this.frmRegHoraExtra.get('dtFinHE')}
  get txtMotivoHE() { return this.frmRegHoraExtra.get('txtMotivoHE')}

}
