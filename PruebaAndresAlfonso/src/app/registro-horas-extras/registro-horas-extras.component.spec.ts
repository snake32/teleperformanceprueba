import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroHorasExtrasComponent } from './registro-horas-extras.component';

describe('RegistroHorasExtrasComponent', () => {
  let component: RegistroHorasExtrasComponent;
  let fixture: ComponentFixture<RegistroHorasExtrasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistroHorasExtrasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroHorasExtrasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
