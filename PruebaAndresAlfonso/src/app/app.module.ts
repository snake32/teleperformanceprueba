import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RegistroHorasExtrasComponent } from './registro-horas-extras/registro-horas-extras.component';

@NgModule({
  declarations: [
    AppComponent,
    RegistroHorasExtrasComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
